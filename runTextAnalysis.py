# coding: utf-8

import sys
##################################################################
#Example Usage:
#python arabic-morphological-analysis-demo.py test-text-files/test-in.ar.txt test-text-files/test-out.ar.txt

def usage():
	print 'Usage: ', sys.argv[0], '<inputfile> '
	print 'python runTextAnalysis.py data.txt'
##################################################################

if len(sys.argv) < 2: usage(); sys.exit(2)


'''
Demo of Arabic morphological analysis tools, which are as follows:

- Text processing : remove Arabic diacritics and punctcutions, and tokenizing 
- Rooting (ISRI Arabic Stemmer)
- light stemming



ISRI Arabic Stemmer is described in:

Taghva, K., Elkoury, R., and Coombs, J. 2005. Arabic Stemming without a root dictionary. Information Science Research Institute. University of Nevada, Las Vegas, USA.

The difference between ISRI Arabic Stemmer and The Khoja stemmer is that ISRI stemmer does not use root dictionary. Also, if a root is not found, ISRI stemmer returned normalized form, rather than returning the original unmodified word.


Light stemming for Arabic words is to remove common affix (prefix and suffix) from words, but it does not convert words into their root form.

'''

import imp
tp = imp.load_source('textpro', 'textpro.py')

##################################################################


if __name__ == "__main__":
    tp.analyzeText(sys.argv)


